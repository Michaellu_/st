{
  description = "Michaellu's custom st terminal";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
  };

  outputs = { self, nixpkgs }:
    let
      system = "x86_64-linux";

      overlay = final: prev: {
        st = prev.st.overrideAttrs (old: {
          version = "0.9.1";
          src = builtins.path { path = ./.; name = "st"; };
        });
      };

      st = (import nixpkgs {
        inherit system;
        overlays = [ overlay ];
      }).st;

    in {
      overlays.default = overlay;

      packages.${system}.default = st;
  };
}
